/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addtwomatrix;

import java.util.Scanner;
import java.util.Scanner;

/**
 *
 * @author BSDE
 */
public class MatrixClass {

    int m, n, c, d;

    public void inputMat(int m, int n) {
        Scanner s = new Scanner(System.in);

        System.out.println("M " + m + " n " + n);

        int First[][] = new int[m][n];
        int Second[][] = new int[m][n];
        int sum[][] = new int[m][n];
        System.out.println("Enter the elements of first matrix");
        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                First[c][d] = s.nextInt();
            }
        }
//        System.out.println(First[c][d]);

        System.out.println("Enter the second matrix");
        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                Second[c][d] = s.nextInt();
            }
        }

        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                sum[c][d] = First[c][d] + Second[c][d];  //replace '+' with '-' to subtract matrices
            }
        }
        System.out.println("Sum of entered matrices:-");

        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                System.out.print(sum[c][d] + "\t");
            }

            System.out.println();
        }
    }

}
